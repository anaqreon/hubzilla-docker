Deploy Hubzilla and Traefik using Docker Compose
=================================================

Overview
-----------------------------------------------

Docker makes deploying a [Hubzilla](http://hubzilla.org) server, or "hub", as simple as it gets. Three Docker containers are created using the docker-compose system. One container runs [Traefik](http://traefik.io), which acts as a reverse proxy and an automatic installer for your hub's [LetsEncrypt](http://letsencrypt.org) SSL certificate. 

The Hubzilla instance consists of a webserver running the Hubzilla source code and a container running the MySQL database.

Requirements
-----------------------------------------------

You need a Linux host machine with [docker-compose](https://docs.docker.com/compose/) installed. It is recommended to run Docker as a non-root user, who is added to the `docker` group. 

Installation
-----------------------------------------------

1. Clone this repo to some directory path we will reference as `$REPO`.
1. Copy `$REPO/hubzilla/sample.env` to `$REPO/hubzilla/.env` and customize for your server configuration.
1. Modify the `$REPO/traefik/traefik.yaml` config file with your own email address and any other customizations you wish to make.
1. Launch Traefik:
    ```
    cd $REPO/traefik
    docker-compose up -d
    ```
1. Launch Hubzilla:
    ```
    cd $REPO/hubzilla
    docker-compose up -d
    ```

In a few minutes your hub should be online with a valid TLS cert from LetsEncrypt.

Notes
-----------------------------------------------

* The self-signed TLS certificate generated in the Dockefile is only needed because Hubzilla will throw an error if it detects that it is running on HTTP, because there is no way to make it aware that it is behind a reverse-proxy like Traefik that is providing the TLS termination. 