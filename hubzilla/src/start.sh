#!/bin/bash -e

if [[ -f /var/www/html/.htconfig.php ]]; then
  echo "Hubzilla is already installed."
else
  echo "Installing Hubzilla..."
  # Construct the hub web root from the official git repositories
  git clone $SOURCE_URL_CORE /var/www/html/
  cd /var/www/html/ && util/add_addon_repo $SOURCE_URL_ADDONS official
  mkdir -p "/var/www/html/store/[data]/smarty3"

  # Generate or update the basic hub config file
  cp /opt/.htconfig.php /var/www/html/.htconfig.php
  if [[ "x$LOCATION_HASH" == "x" ]]; then
    LOCATION_HASH=$(cat /dev/urandom | tr -dc 'a-f0-9' | fold -w 64 | head -n 1)
  fi
  sed -i "s/{{LOCATION_HASH}}/$LOCATION_HASH/g" /var/www/html/.htconfig.php && \
  sed -i "s/{{DOMAIN}}/$DOMAIN/g" /var/www/html/.htconfig.php && \
  sed -i "s/{{SITE_NAME}}/$SITE_NAME/g" /var/www/html/.htconfig.php && \
  sed -i "s#{{TIMEZONE}}#$TIMEZONE#g" /var/www/html/.htconfig.php && \
  sed -i "s/{{ADMIN_EMAIL}}/$ADMIN_EMAIL/g" /var/www/html/.htconfig.php
  sed -i "s/{{MYSQL_USER}}/$MYSQL_USER/g" /var/www/html/.htconfig.php
  sed -i "s/{{MYSQL_PASSWORD}}/$MYSQL_PASSWORD/g" /var/www/html/.htconfig.php
  sed -i "s/{{MYSQL_DATABASE}}/$MYSQL_DATABASE/g" /var/www/html/.htconfig.php
  
  chown -R www-data:www-data /var/www/html/
fi

if [[ -f /var/lib/mysql/hubzilla/account.frm ]]; then
  echo "Database is already installed."
else
  echo "Installing database..."
  # Initialize the database tables
  mysql \
    --host="db" \
    --user="${MYSQL_USER}" \
    --password="${MYSQL_PASSWORD}" \
    --database="${MYSQL_DATABASE}" \
    < /var/www/html/install/schema_mysql.sql
fi

cd /var/www/html
util/udall

/usr/sbin/apache2ctl -D FOREGROUND
